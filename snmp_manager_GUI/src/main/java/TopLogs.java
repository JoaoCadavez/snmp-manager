import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TopLogs {
    private LogReader logReader;

    public TopLogs() {
        logReader = new LogReader();
    }

    public Map<String, List<Log>> getTopProcess(String host, LocalDateTime dateIni, LocalDateTime dateFin, int numberOfTop, String processType) throws IOException {
        Map<String, List<Log>> logsFromHost = new TreeMap<>();
        //for (var host : hosts) {
            var logs = logReader.getLogsByDateByHost(host, dateIni, dateFin);
            logsFromHost.put(host, logs);
        //}
        return selectTop(logsFromHost, numberOfTop, processType);
    }

    private Map<String, List<Log>> selectTop(Map<String, List<Log>> logsFromHost, int numberOfTop, String processType) {
        Map<String, List<Log>> theTopLog = new TreeMap<>();
        for (var host : logsFromHost.keySet()) {
            Map<String, Integer> logProcessLoad = new TreeMap<>();
            for (var log : logsFromHost.get(host)) {
                if (logProcessLoad.containsKey(log.process_ID) && log.process_type.equals(processType)) {
                    var value = logProcessLoad.get(log.process_ID) + Integer.parseInt(log.log_report);
                    logProcessLoad.put(log.process_ID, value);
                } else if(log.process_type.equals(processType)){
                    logProcessLoad.put(log.process_ID, Integer.parseInt(log.log_report));
                }
            }
            var topProcessID = getMaxProcessIDFromMap(logProcessLoad, numberOfTop);
            var topLogFromHostList = getMaxProcessFromMap(logsFromHost.get(host), topProcessID, processType);
            theTopLog.put(host, topLogFromHostList);
        }
        return theTopLog;
    }

    public List<String> getMaxProcessIDFromMap(Map<String, Integer> map, int qty) {
        List<String> max = new ArrayList<>();
        List<Map.Entry<String, Integer>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        var c = result.keySet();
        for (int i = 0; i < qty; i++) {
            if (i < c.size()) {
                max.add((String) c.stream().toArray()[c.size() - qty + i]);
            }
        }
        return max;
    }
    public List<Log> getMaxProcessFromMap(List<Log> logList, List<String> topProcessID, String processType){
        List<Log> topLogFromHostList = new ArrayList<>();
        for(Log theLog : logList){
            if(theLog.process_type.equals(processType)&&topProcessID.contains(theLog.process_ID)){
                topLogFromHostList.add(theLog);
            }
        }
        return topLogFromHostList;
    }
}
