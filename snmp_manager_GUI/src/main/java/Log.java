import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class Log {

    public String host_ip;

    public String process_ID;

    public String process_name;

    public String log_report;

    public String timestamp;

    public String process_type;

    public Log(String host_ip, String process_ID, String log_report, String process_name, String process_type, String timestamp) {
        this.host_ip = host_ip;
        this.process_ID = process_ID;
        this.log_report = log_report;
        this.process_name= process_name;
        this.process_type = process_type;
        this.timestamp = timestamp;
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
            string.append(this.host_ip);
            string.append(" ");
            string.append(this.process_ID);
            string.append(" ");
            string.append(this.process_type);
            string.append(" ");
            string.append(this.process_name);
            string.append(" ");
            string.append(this.log_report);
            string.append(" ");
            string.append(this.timestamp);
            string.append("\n");

        return string.toString();
    }
    public LocalDateTime getLocalDateTime(){
        var date = this.timestamp.split("_");
        var day = date[1].split("/");
        var time = date[0].split(":");
        return LocalDateTime.of(Integer.parseInt(day[2]), Integer.parseInt(day[1]),Integer.parseInt(day[0]), Integer.parseInt(time[0]),Integer.parseInt(time[1]),Integer.parseInt(time[2]));
    }
}
