import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

public class GUIBuilder implements ActionListener {
    private JLabel errorLabel;
    private JButton submitButton;
    private JFrame frame;
    private JPanel panel;
    private JComboBox processList;
    private String[] processType = {"cpu", "ram"};
    private JComboBox numberOfTop;
    private String[] numberOfTopList = {"1", "5", "10", "20"};
    private JTextField dateIni;
    private JTextField dateFin;
    private JComboBox hostList;
    private TopLogs topLogs;
    private String[] hostArray;

    private String processString = "";
    private String numberOfTopString = "";
    private String dateIniString = "";
    private String dateFinString = "";

    public GUIBuilder() {
        baseBuild();
        dropDownTable();
        addAllObject();
        frameSettings();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    private void baseBuild() {
        frame = new JFrame();

        submitButton = new JButton(new AbstractAction("Submit") {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!dateIni.getText().isEmpty() && !dateIni.getText().equals("Initial Date") &&
                        !dateFin.getText().isEmpty() && !dateFin.getText().equals("Final Date") &&
                        numberOfTop.getSelectedIndex() > -1 &&
                        hostList.getSelectedIndex() > -1 &&
                        processList.getSelectedIndex() > -1) {
                    topLogs = new TopLogs();

                    LocalDateTime dateIniLDT = getDateFromString(dateIni);
                    LocalDateTime dateFinLDT = getDateFromString(dateFin);
                    try {
                        var x = topLogs.getTopProcess(hostArray[hostList.getSelectedIndex()],
                                dateIniLDT,
                                dateFinLDT,
                                Integer.parseInt(numberOfTopList[numberOfTop.getSelectedIndex()]),
                                processType[processList.getSelectedIndex()]);
                        var y = x.get(hostArray[hostList.getSelectedIndex()]);
                        ChartBuilder chartBuilder = new ChartBuilder();
                        var imagePath = chartBuilder.createTimeChart(hostArray[hostList.getSelectedIndex()], y, Integer.parseInt(numberOfTopList[numberOfTop.getSelectedIndex()]));
                        createImageWindow(imagePath);
                        errorLabel.setText("Success!");
                    } catch (IOException ioException) {
                        errorLabel.setText("Error Saving the image...");
                    } catch (IllegalArgumentException f) {
                        errorLabel.setText("Data not found for this settings");
                    } catch (Exception f) {
                        System.out.println(f.toString());
                        errorLabel.setText("Something went wrong...");
                    }
                } else {
                    errorLabel.setText("We Found A Problem! Please Check All The Fields!");
                }
            }
        });

        errorLabel = new JLabel();

        dateIni = new JTextField("Initial Date: yyyy-MM-ddThh:mm:SS");
        dateFin = new JTextField("Final Date: yyyy-MM-ddThh:mm:SS");
        textBoxOnClick(dateIni);
        textBoxOnClick(dateFin);

        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        panel.setLayout(new GridLayout(0, 1));

    }

    private void dropDownTable() {
        processList = new JComboBox(processType);
        processList.setRenderer(new MyComboBoxRenderer("Process Type"));
        processList.setSelectedIndex(-1);
        processList.addActionListener(this);
        numberOfTop = new JComboBox(numberOfTopList);
        numberOfTop.setRenderer(new MyComboBoxRenderer("Top Process Selector"));
        numberOfTop.setSelectedIndex(-1);
        numberOfTop.addActionListener(this);
        LogReader log = new LogReader();
        hostArray = log.getAllHost().toArray(new String[0]);
        hostList = new JComboBox(hostArray);
        hostList.setRenderer(new MyComboBoxRenderer("Host List"));
        hostList.setSelectedIndex(-1);
        hostList.addActionListener(this);
    }

    private void frameSettings() {
        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("SNMP Manager GUI");
        frame.setVisible(true);
        frame.pack();
        frame.setSize(500, 500);
    }

    private void addAllObject() {
        panel.add(processList);
        panel.add(numberOfTop);
        panel.add(hostList);
        panel.add(dateIni);
        panel.add(dateFin);
        panel.add(submitButton);
        panel.add(errorLabel);
    }

    private LocalDateTime getDateFromString(JTextField dateString) {
        var dateStringSplit = dateString.getText().split("T");
        var day = dateStringSplit[0].split("-");
        var hour = dateStringSplit[1].split(":");
        return LocalDateTime.of(Integer.parseInt(day[0]), Integer.parseInt(day[1]), Integer.parseInt(day[2]), Integer.parseInt(hour[0]), Integer.parseInt(hour[1]), Integer.parseInt(hour[2]));
    }

    public void createImageWindow(String imagePath) {
        JFrame f = new JFrame(); //creates jframe f

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(imagePath);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); //this is your screen size

        //f.setUndecorated(true); //removes the surrounding border

        ImageIcon image = new ImageIcon("./chart/" + imagePath); //imports the image

        JLabel lbl = new JLabel(image); //puts the image into a jlabel

        f.getContentPane().add(lbl); //puts label inside the jframe

        f.setSize(image.getIconWidth(), image.getIconHeight() + 50); //gets h and w of image and sets jframe to the size

        int x = (screenSize.width - f.getSize().width) / 2; //These two lines are the dimensions
        int y = (screenSize.height - f.getSize().height) / 2;//of the center of the screen

        f.setLocation(x, y); //sets the location of the jframe
        f.setVisible(true); //makes the jframe visible
    }

    private void textBoxOnClick(JTextField text) {
        text.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                text.setText("");
            }
        });
    }

    class MyComboBoxRenderer extends JLabel implements ListCellRenderer {
        private String _title;

        public MyComboBoxRenderer(String title) {
            _title = title;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean hasFocus) {
            if (index == -1 && value == null) setText(_title);
            else setText(value.toString());
            return this;
        }
    }
}
