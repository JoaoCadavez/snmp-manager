import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ChartBuilder {

    private final int chartEntries = 50;

    public String createTimeChart(String chartName, List<Log> logList, int numberTop) throws IOException {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        String processType = "";
        logList = getSmallList(logList, numberTop);
        for (var log : logList) {
            processType = log.process_type;
            dataset.addValue(Double.parseDouble(log.log_report), log.process_name + "_" + log.process_ID, log.getLocalDateTime());
        }
        String plotTitle = "SNMP Chart";
        String xaxis = "Process Name";
        String yaxis = "value " + processType;
        if (processType.equalsIgnoreCase("cpu")) {
            yaxis = "value " + processType + " %";
        } else if (processType.equalsIgnoreCase("ram")) {
            yaxis = "value " + processType + " KB";
        }
        PlotOrientation orientation = PlotOrientation.VERTICAL;
        boolean show = true;
        boolean toolTips = true;
        boolean urls = false;
        JFreeChart chart = ChartFactory.createLineChart(plotTitle, xaxis, yaxis,
                dataset, orientation, show, toolTips, urls);
        chart.getCategoryPlot().getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_90);

        int width = 20 * logList.size() / numberTop;
        int height = 700;
        var size = findDiferentLogProcessID(logList).size();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        StringBuilder theTime = new StringBuilder();
        theTime.append(timestamp.toLocalDateTime().getYear());
        theTime.append("-");
        theTime.append(timestamp.toLocalDateTime().getMonthValue());
        theTime.append("-");
        theTime.append(timestamp.toLocalDateTime().getDayOfMonth());
        theTime.append("_");
        theTime.append(timestamp.toLocalDateTime().getHour());
        theTime.append(":");
        theTime.append(timestamp.toLocalDateTime().getMinute());
        theTime.append(":");
        theTime.append(timestamp.toLocalDateTime().getSecond());
        String returnPath = chartName + "_" + processType + "_" + size + "_" + theTime.toString() + ".PNG";
        String thePath = "./chart/" + returnPath;
        try {
            ChartUtilities.saveChartAsPNG(new File(thePath), chart, width, height);
        } catch (IOException e) {
            throw new IOException();
        }
        return returnPath;
    }

    private List<Log> getSmallList(List<Log> logList, int numberTop) {
        int iterationValue = logList.size() / chartEntries;
        List<Log> newLogList = new ArrayList<>();
        List<Log> auxLogList = new ArrayList<>();
        int key = 0;
        for (int i = 0; i < chartEntries; i++) {
            if (key < logList.size()) {
                auxLogList.add(logList.get(key));
            }
            key += iterationValue;
        }
        for (var auxLog : auxLogList) {
            for (var log : logList) {
                if (log.timestamp.equalsIgnoreCase(auxLog.timestamp)) {
                    newLogList.add(log);
                }
            }
        }
        return newLogList;
    }

    private List<String> findDiferentLogProcessID(List<Log> logList) {
        List<String> uniqueProcessID = new ArrayList<>();
        for (var log : logList) {
            if (!uniqueProcessID.contains(log.process_ID)) {
                uniqueProcessID.add(log.process_ID);
            }
        }
        return uniqueProcessID;
    }
}
