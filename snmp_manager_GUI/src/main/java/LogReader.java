import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class LogReader {
    private String log_file_path = "./../snmp_manager_engine/log_report/";


    private String text_interpreter(String file_path) throws IOException {
        File file = new File(file_path);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();
        return new String(data, "UTF-8");
    }

    private List<Log> process_log(String data, LocalDateTime dateIni, LocalDateTime dateFin) {
        List<Log> log_list = new ArrayList<>();
        var data_split = data.split("\n");
        for (var line : data_split) {
            if (!line.isEmpty()) {
                var cell = line.split("&");
                String host_ip = cell[0];
                String process_type = cell[1];
                String process_ID = cell[2];
                String process_name = cell[3];
                String process_value = cell[4];
                String timestamp = cell[5];
                LocalDateTime theTimeStamp = getTimeStampFromString(timestamp);
                if (dateIni.compareTo(theTimeStamp) <= 0 && dateFin.compareTo(theTimeStamp) >= 0) {
                    Log new_log = new Log(host_ip, process_ID, process_value, process_name, process_type, timestamp);
                    log_list.add(new_log);
                }

            }
        }
        return log_list;
    }

    private LocalDateTime getTimeStampFromString(String timestamp) {
        var hour_day = timestamp.split("_");
        var time = hour_day[0].split(":");
        var day = hour_day[1].split("/");
        return LocalDateTime.of(Integer.parseInt(day[2]), Integer.parseInt(day[1]), Integer.parseInt(day[0]), Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2]));
    }

    private List<String> getAllLogsFilenameFromHost(String host, LocalDateTime dateIni, LocalDateTime dateFin) {
        File folder = new File(log_file_path + "/" + host);
        File[] listOfFiles = folder.listFiles();
        List<String> filenameList = new ArrayList<>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                var dateSplit = listOfFiles[i].getName().split("_");
                var fileDate = LocalDate.of(Integer.parseInt(dateSplit[2]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[0]));
                if (dateIni.toLocalDate().compareTo(fileDate) <= 0 && dateFin.toLocalDate().compareTo(fileDate) >= 0) {
                    filenameList.add(listOfFiles[i].getName());
                }

            }
        }
        return filenameList;
    }

    public List<Log> getLogsByDateByHost(String host, LocalDateTime dateIni, LocalDateTime dateFin) throws IOException {
        var filenameList = getAllLogsFilenameFromHost(host, dateIni, dateFin);
        List<Log> allLogs = new ArrayList<>();
        for (var filename : filenameList) {
            var raw_logs = text_interpreter(log_file_path + "/" + host + "/" + filename);
            var logList = process_log(raw_logs, dateIni, dateFin);
            allLogs.addAll(logList);
        }
        return allLogs;
    }

    public List<String> getAllHost() {
        List<String> host = new ArrayList<>();
        File folder = new File(log_file_path);
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isDirectory()) {
                host.add(listOfFiles[i].getName());
            }
        }
        return host;
    }

}
