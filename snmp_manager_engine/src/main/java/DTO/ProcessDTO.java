package DTO;

import java.util.List;

public class ProcessDTO {
    public String host_IP;
    public String host_name;
    public String port;
    public String comm_string;
    public String snmp_version;
    public String polling_freq;
    public List<String> process;
    private String default_port="161";

    public ProcessDTO(){}
    public ProcessDTO(String new_host_IP, String new_host_name, String new_port, String new_comm_string, String new_SNMP_version, String new_polling_freq, List<String> new_process){
        host_IP=new_host_IP;
        host_name=new_host_name;
        port = new_port;
        comm_string=new_comm_string;
        snmp_version=new_SNMP_version;
        polling_freq=new_polling_freq;
        process=new_process;
    }
    public ProcessDTO(String new_host_IP, String new_host_name, String new_comm_string, String new_snmp_version, String new_polling_freq, List<String> new_process){
        host_IP=new_host_IP;
        host_name=new_host_name;
        port = default_port;
        comm_string=new_comm_string;
        snmp_version = new_snmp_version;
        polling_freq=new_polling_freq;
        process=new_process;
    }
}
