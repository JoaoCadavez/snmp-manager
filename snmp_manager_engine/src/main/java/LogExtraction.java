import Objects.SNMPProcess;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.util.TreeEvent;

import java.util.*;

public class LogExtraction {
    private SnmpWalk snmpWalk;

    public LogExtraction(SNMPProcess snmpProcess) {
        snmpWalk = new SnmpWalk(snmpProcess);
    }

    public Map<String, List<String>> getLogReport(String hrSWRunName, String anotherMIB) throws Exception {
        List<TreeEvent> process_name = snmpWalk.snmpRequest(hrSWRunName);
        List<TreeEvent> process_info = snmpWalk.snmpRequest(anotherMIB);
        var process_name_info = getInfo(process_name);
        var process_perf_info = getInfo(process_info);
        getInfo(process_info);
        return joinMaps(process_name_info, process_perf_info);
    }

    private Map<String, List<String>> joinMaps(Map<String, String> process_name, Map<String, String> process_perf) {
        Map<String, List<String>> newMap = new TreeMap<>();
        for (String index : process_name.keySet()) {
            String proc_perf = process_perf.get(index);
            String proc_name = process_name.get(index);
            List<String> args = new ArrayList<>();
            args.add(proc_name);
            args.add(proc_perf);
            newMap.put(index, args);
        }
        return newMap;
    }

    private Map<String, String> getInfo(List<TreeEvent> theNameTreeList) {
        Map<String, String> nameMap = new TreeMap<>();
        try {
            for (TreeEvent theTreeEvent : theNameTreeList) {
                for (VariableBinding event : theTreeEvent.getVariableBindings()) {
                    String oid = event.getOid().toString();
                    String iodIndex = oid.substring(oid.lastIndexOf(".") + 1);
                    String vari = event.getVariable().toString();
                    nameMap.put(iodIndex, vari);
                }
            }
        } catch (Exception e) {
            System.out.println("Host Unreachable...");
        }
        return nameMap;
    }

}
