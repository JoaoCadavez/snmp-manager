package LogPackage;

import Objects.SNMPProcess;
import org.snmp4j.util.TreeEvent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LogManager {
    private LogReporter logReport;
    private Path path = Paths.get("./log_report");

    public LogManager() {
        logReport = new LogReporter();
    }

    public void LogtoString(SNMPProcess snmpProcess, Map<String, List<String>> rawLog, String process_type) throws IOException {
        List<String> result_report = new ArrayList<>();
        for (var x : rawLog.keySet()) {
            StringBuilder log_result = new StringBuilder();
            log_result.append(x);
            log_result.append("&");
            int info_length = rawLog.get(x).size();
            int count=0;
            for(var info : rawLog.get(x)){
                log_result.append(info);
                if(count<info_length-1){
                    log_result.append("&");
                }
                count++;
            }
            result_report.add(log_result.toString());
        }
        logReport.saveLog(result_report, snmpProcess, getFilePathToLog(snmpProcess), process_type);
    }

    private String getFilePathToLog(SNMPProcess snmpProcess) throws IOException {
        if (!checkIfFolderExist(snmpProcess)) {
            createFolder(snmpProcess);
        }
        if (!checkIfFileExists(snmpProcess)) {
            createFile(snmpProcess);
        }
        return getFilePath(snmpProcess);
    }

    private boolean checkIfFolderExist(SNMPProcess snmpProcess) {
        String folder_path = getFolderPath(snmpProcess);
        if (Files.exists(Path.of(folder_path))) {
            return true;
        }
        return false;
    }

    private boolean createFolder(SNMPProcess snmpProcess) throws IOException {
        Path folder_path = Path.of(getFolderPath(snmpProcess));
        try {
            Files.createDirectory(folder_path);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean checkIfFileExists(SNMPProcess snmpProcess) {
        File f = new File(getFilePath(snmpProcess));
        if (f.exists()) {
            return true;
        }
        return false;
    }

    private boolean createFile(SNMPProcess snmpProcess) throws IOException {
        File newFile = new File(getFilePath(snmpProcess));
        return newFile.createNewFile();
    }

    private String getDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    private String getFolderPath(SNMPProcess snmpProcess) {
        return path + "/" + snmpProcess.ToDTO().host_IP;
    }

    private String getFilePath(SNMPProcess snmpProcess) {
        return getFolderPath(snmpProcess) + "/" + getDate();
    }
}
