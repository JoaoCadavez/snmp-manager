package LogPackage;

import DTO.ProcessDTO;
import Objects.Log;
import Objects.SNMPProcess;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class LogReporter {
    public LogReporter(){
    }
    public void saveLog(List<String> logs, SNMPProcess process, String filePathToLog, String process_type) throws IOException {
        ProcessDTO theDTO = process.ToDTO();
        Log new_log = new Log(theDTO.host_IP, logs, process_type);
        saveToFile(new_log, filePathToLog);
    }
    private void saveToFile(Log theLog, String theFilePath) throws IOException {
        FileWriter fileWriter = new FileWriter(theFilePath, true);
        fileWriter.write(theLog.toString()+"\n");
        fileWriter.close();
    }

}
