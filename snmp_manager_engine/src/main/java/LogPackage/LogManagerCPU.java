package LogPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LogManagerCPU {
    private int CPUNanoClock = 0;
    private Map<String, List<String>> iterationLog;

    public LogManagerCPU() {
        iterationLog = new TreeMap<>();
    }

    public Map<String, List<String>> cpuRawToLogs(Map<String, List<String>> rawLogs) {
        Map<String, List<String>> newLogs = new TreeMap<>();
        if (iterationLog.isEmpty()) {
            iterationLog = rawLogs;
            return new TreeMap<>();
        } else {
            CPUNanoClock = 0;
            for (var x : rawLogs.keySet()) {
                try {
                    var lastLog = iterationLog.get(x);
                    var lastLogNano = lastLog.get(1);
                    var presentLogNano = rawLogs.get(x).get(1);
                    CPUNanoClock = CPUNanoClock + (Integer.parseInt(presentLogNano) - Integer.parseInt(lastLogNano));
                } catch (Exception e) {

                }
            }
        }
        for (var x : rawLogs.keySet()) {
            try {
                var lastLog = iterationLog.get(x);
                var lastLogNano = lastLog.get(1);
                var presentLogNano = rawLogs.get(x).get(1);
                int nanoDif = (Integer.parseInt(presentLogNano) - Integer.parseInt(lastLogNano));
                int percentage = 0;
                if(nanoDif!=0 && CPUNanoClock!=0){
                    percentage = (100 * nanoDif) / CPUNanoClock;
                }
                List<String> theList = new ArrayList<>();
                theList.add(rawLogs.get(x).get(0));
                theList.add(String.valueOf(percentage));
                newLogs.put(x, theList);
            } catch (Exception e) {

            }

        }
        return newLogs;
    }
}
