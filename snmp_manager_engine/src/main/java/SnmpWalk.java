import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import DTO.ProcessDTO;
import Objects.SNMPProcess;
import org.snmp4j.*;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

public class SnmpWalk {
    private ProcessDTO processDTO;
    public SnmpWalk(SNMPProcess process){
        processDTO = process.ToDTO();
    }
    public List<TreeEvent> snmpRequest(String oid) throws Exception {
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString(processDTO.comm_string));
        String address = "udp:"+processDTO.host_IP+"/"+processDTO.port;
        target.setAddress(GenericAddress.parse(address));
        target.setRetries(2);
        target.setTimeout(1500);
        target.setVersion(Integer.parseInt(processDTO.snmp_version));
        return doWalk(oid, target);
    }

    public List<TreeEvent> doWalk(String tableOid, Target target) throws IOException {
        Map<String, String> result = new TreeMap<>();
        TransportMapping<? extends Address> transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        var tree = treeUtils.getSubtree(target, new OID(tableOid));
        snmp.close();
        return tree;
    }

}