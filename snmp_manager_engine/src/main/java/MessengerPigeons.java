import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;
import java.util.Properties;

public class MessengerPigeons {

    private String username = "SNMP@sapo.pt";
    private String password = "Teste123!";

    private String from = "SNMP@sapo.pt";

    private String subject = "New Alert!!";

    private ConfFileReader theConfFileReader;

    public void sendAlert(List<String> theListAlerts) {
        theConfFileReader = new ConfFileReader();
        String to = theConfFileReader.getDestinationAddress();
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.sapo.pt");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.ssl.trust", "smtp.sapo.pt");
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);

            String theMessage="";
            for(var theAlert : theListAlerts){
                theMessage+=theAlert+"\n";
            }

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(theMessage,"text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);
            System.out.println("A New Alert Has Sent To: "+to);
            Transport.send(message);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
