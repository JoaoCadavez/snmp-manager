package Objects;

import DTO.ProcessDTO;

import java.util.List;

public class SNMPProcess {
    private String host_IP;
    private String host_name;
    private String host_port;
    private String comm_string;
    private String snmp_version;
    private String polling_freq;
    private List<String> process_list;
    private String default_port = "161";

    public SNMPProcess(ProcessDTO processDTO) {
        FromDTO(processDTO);
    }

    public SNMPProcess(String new_host_IP, String new_host_name, String new_host_port, String new_comm_string, String new_SNMP_version, String new_polling_freq, List<String> new_process) {
        host_IP = new_host_IP;
        host_name = new_host_name;
        host_port = new_host_port;
        comm_string = new_comm_string;
        snmp_version = new_SNMP_version;
        polling_freq = new_polling_freq;
        process_list = new_process;
    }

    public SNMPProcess(String new_host_IP, String new_host_name, String new_comm_string, String new_SNMP_version, String new_polling_freq, List<String> new_process) {
        host_IP = new_host_IP;
        host_name = new_host_name;
        host_port = default_port;
        comm_string = new_comm_string;
        snmp_version = new_SNMP_version;
        polling_freq = new_polling_freq;
        process_list = new_process;
    }

    private void FromDTO(ProcessDTO processDTO) {
        this.host_IP = processDTO.host_IP;
        this.host_name = processDTO.host_name;
        this.host_port = processDTO.port;
        this.comm_string = processDTO.comm_string;
        this.snmp_version = processDTO.snmp_version;
        this.polling_freq = processDTO.polling_freq;
        this.process_list = processDTO.process;
    }

    public ProcessDTO ToDTO() {
        return new ProcessDTO(host_IP, host_name, host_port, comm_string, snmp_version, polling_freq, process_list);
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o instanceof SNMPProcess) {
            SNMPProcess theObject = (SNMPProcess) o;
            if (this.host_name.equals(theObject.host_name) &&
                    this.host_IP.equals(theObject.host_IP) &&
                    this.polling_freq.equals(theObject.polling_freq) &&
                    this.comm_string.equals(theObject.comm_string) &&
                    this.host_port.equals(theObject.host_port) &&
                    this.snmp_version.equals(theObject.snmp_version) &&
                    this.process_list.equals(theObject.process_list)) {
                return true;
            }
        }
        return false;
    }
}
