package Objects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Log {

    private String host_ip;

    private List<String> log_report;

    private String timestamp;

    private String process_type;

    public Log(String host_ip, List<String> log_report, String process_type) {
        this.host_ip = host_ip;
        this.log_report = log_report;
        this.process_type = process_type;
        this.timestamp = getTimeStamp();
    }

    private String getTimeStamp() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss_dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
        for (var line : this.log_report) {
            string.append(this.host_ip);
            string.append("&");
            string.append(this.process_type);
            string.append("&");
            string.append(line);
            string.append("&");
            string.append(this.timestamp);
            string.append("\n");
        }
        return string.toString();
    }
}
