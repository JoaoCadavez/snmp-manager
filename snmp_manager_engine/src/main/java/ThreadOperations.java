import Objects.SNMPProcess;

import java.util.Set;

public class ThreadOperations {
    public void createThread(SNMPProcess process){
        ProcessThread newThread = new ProcessThread(process);
        startThread(newThread);
    }
    private void startThread(Thread theThread){
        theThread.start();
    }
    private Set<Thread> runningThreads(){
        return Thread.getAllStackTraces().keySet();
    }
    public void killThread(String threadName){
        Set<Thread> setOfThread = runningThreads();
        for(Thread thread : setOfThread){
            if(thread.getName()==threadName){
                thread.interrupt();
            }
        }
    }

}
