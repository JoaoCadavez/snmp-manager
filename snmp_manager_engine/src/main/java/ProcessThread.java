import DTO.ProcessDTO;
import LogPackage.LogManager;
import LogPackage.LogManagerCPU;
import Objects.SNMPProcess;

public class ProcessThread extends Thread {
    private ProcessDTO processDTO;
    private SnmpWalk snmpWalk;
    private LogManager logManager;
    private SNMPProcess snmpProcess;
    private LogExtraction logExtraction;
    private String hrSWRunName = "1.3.6.1.2.1.25.4.2.1.2";
    private String hrSWRunPerfCPU = "1.3.6.1.2.1.25.5.1.1.1";
    private String hrSWRunPerfRAM = "1.3.6.1.2.1.25.5.1.1.2";
    private LogManagerCPU logManagerCPU;
    private AlertSNMP theAlert;
    private boolean flag =true;

    public ProcessThread(SNMPProcess process) {
        snmpProcess = process;
        processDTO = process.ToDTO();
        this.setName(processDTO.host_IP);
        this.snmpWalk = new SnmpWalk(process);
        this.logManager = new LogManager();
        this.logExtraction = new LogExtraction(process);
        logManagerCPU = new LogManagerCPU();
        theAlert = new AlertSNMP();
    }

    public void run() {
        while (flag) {
            try {
                for (String processAnalyses : processDTO.process) {
                    switch (processAnalyses.toLowerCase()) {
                        case ("cpu"):
                            var cpuresult = logExtraction.getLogReport(hrSWRunName, hrSWRunPerfCPU);
                            var processLogs = logManagerCPU.cpuRawToLogs(cpuresult);
                            if (!processLogs.isEmpty()) {
                                logManager.LogtoString(snmpProcess, processLogs, "cpu");
                                theAlert.verifySNMPValues(snmpProcess, processLogs, "cpu");
                            }
                            break;
                        case ("ram"):
                            var ramresult = logExtraction.getLogReport(hrSWRunName, hrSWRunPerfRAM);
                            logManager.LogtoString(snmpProcess, ramresult, "ram");
                            theAlert.verifySNMPValues(snmpProcess, ramresult, "ram");
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000 * Integer.parseInt(processDTO.polling_freq));
            } catch (InterruptedException e) {
                flag = false;
            }
        }
        Thread.interrupted();
    }
}
