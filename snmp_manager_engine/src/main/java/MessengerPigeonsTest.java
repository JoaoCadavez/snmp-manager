import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


class MessengerPigeonsTest {

    @Test
    void sendAlert() {
        List<String> testList = new ArrayList<>();
        testList.add("Teste");
        testList.add("OtherTest");
        MessengerPigeons messengerPigeons = new MessengerPigeons();
        messengerPigeons.sendAlert(testList);
    }
}