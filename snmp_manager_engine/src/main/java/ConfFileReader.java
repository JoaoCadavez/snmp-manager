import DTO.ProcessDTO;
import Objects.SNMPProcess;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ConfFileReader {

    private String conf_file_manager = "./conf_file.json";
    private String conf_file_alert = "./alert_file.json";

    private JSONObject json_interpreter(String path) {
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(path));
            return (JSONObject) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<SNMPProcess> ProcessDTOList() {
        List<SNMPProcess> SNMPProcessList = new ArrayList<>();
        JSONObject jsonObject = json_interpreter(conf_file_manager);
        JSONArray hostList = (JSONArray) jsonObject.get("hosts");
        for (Object host : hostList) {
            if (host instanceof JSONObject) {
                JSONObject hostObject = (JSONObject) host;
                String host_ip = (String) hostObject.get("IP");
                String host_name = (String) hostObject.get("Name");
                String host_polling = (String) hostObject.get("Polling");
                String comm_string = (String) hostObject.get("CommunityString");
                String port = (String) hostObject.get("Port");
                String snmp_version = (String) hostObject.get("SNMP_version");
                List<String> host_process = (List<String>) hostObject.get("Process");
                if (ValidInput(host_ip, host_name, comm_string, snmp_version, host_polling, host_process)) {
                    ProcessDTO processDTO;
                    if (port.isEmpty() || port == null) {
                        processDTO = new ProcessDTO(host_ip, host_name, comm_string, snmp_version, host_polling, host_process);
                    } else {
                        processDTO = new ProcessDTO(host_ip, host_name, port, comm_string, snmp_version, host_polling, host_process);
                    }
                    SNMPProcessList.add(new SNMPProcess(processDTO));
                }
            }
        }
        return SNMPProcessList;
    }

    private boolean ValidInput(String host_IP, String host_name, String host_polling, String comm_string, String snmp_version, List<String> host_process) {
        if (host_IP.isEmpty() || host_name.isEmpty() || host_polling.isEmpty() || comm_string.isEmpty() || snmp_version.isEmpty() || host_process.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public Map<String, Integer> ImportAlertSNMPConf() {
        JSONObject theAlertConf = json_interpreter(conf_file_alert);
        Map<String, Integer> theAlertConfMap = new TreeMap<>();
        try{
            JSONArray processList = (JSONArray) theAlertConf.get("processList");

            for (Object process : processList) {
                JSONObject processObject = (JSONObject) process;
                String processName = (String) processObject.get("ProcessName");
                Integer processValue = Integer.parseInt((String) processObject.get("Value"));
                theAlertConfMap.put(processName, processValue);
            }
            if (theAlertConfMap.isEmpty()) {
                theAlertConfMap.put("NONE", 0);
            }
        }catch(Exception e){
            System.out.println("Problem Importing SNMP Alert Config...");
            theAlertConfMap.put("NONE", 100);
        }
        return theAlertConfMap;
    }

    public String getDestinationAddress(){
        JSONObject theAlertConf = json_interpreter(conf_file_alert);
        return (String) theAlertConf.get("mailDestination");
    }
}
