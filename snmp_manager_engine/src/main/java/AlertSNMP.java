import Objects.SNMPProcess;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AlertSNMP {

    private ConfFileReader theConfFileReader = new ConfFileReader();
    private MessengerPigeons theMessengerPigeons = new MessengerPigeons();

    public void verifySNMPValues(SNMPProcess theSNMPProcess, Map<String, List<String>> theRawLogs, String processType) {
        Map<String, Integer> theProcessAlertMap = theConfFileReader.ImportAlertSNMPConf();
        List<String> theListAlerts = new ArrayList<>();
        for (var theProcessAlert : theProcessAlertMap.keySet()) {
            Integer theProcessAlertValue = theProcessAlertMap.get(theProcessAlert);
            for (var theProcessPID : theRawLogs.keySet()) {
                List<String> theLogValues = theRawLogs.get(theProcessPID);
                if (processType.equalsIgnoreCase(theProcessAlert) && Integer.parseInt(theLogValues.get(1)) >= theProcessAlertValue) {
                    String theInformation = "Machine IP: " + theSNMPProcess.ToDTO().host_IP + " | Process Type: " + processType + " | Process Name/PID: " + theLogValues.get(0) + "/" + theProcessPID + " | Value: " + theLogValues.get(1);
                    theListAlerts.add(theInformation);
                }
            }
        }
        if (!theListAlerts.isEmpty()) {
            theMessengerPigeons.sendAlert(theListAlerts);
        }
    }
}
