import Objects.SNMPProcess;

import java.util.List;
import java.util.TimerTask;

public class ThreadManager extends TimerTask {
    private List<SNMPProcess> lastProcessList;
    private ConfFileReader confFileReader;
    private ThreadOperations threadOperations;

    public ThreadManager() {
        confFileReader = new ConfFileReader();
        threadOperations = new ThreadOperations();
        this.run();
    }

    public void run() {
        try {
            fileChecker();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void fileChecker() throws InterruptedException {
        while (true) {
            List<SNMPProcess> processList = confFileReader.ProcessDTOList();
            if (lastProcessList == null) {
                if (processList != null) {
                    for (SNMPProcess newProcess : processList) {
                        System.out.println("New Process Started: "+newProcess.ToDTO().host_name+" | "+newProcess.ToDTO().host_IP);
                        threadOperations.createThread(newProcess);
                    }
                }
                lastProcessList = processList;
            }
            if (!processList.equals(lastProcessList) && processList != null) {
                checkIfAdded(processList, lastProcessList);
                checkIfDeletedOrUpdated(processList, lastProcessList);
                lastProcessList = processList;
            }
            Thread.sleep(10 * 1000);
        }
    }

    protected void checkIfUpdated(List<SNMPProcess> processList, SNMPProcess oldProcess) {
        if (!processList.contains(oldProcess)) {
            threadOperations.killThread(oldProcess.ToDTO().host_IP);
            SNMPProcess theNewProcessThread = processList.stream().filter(o -> o.ToDTO().host_IP.equals(oldProcess.ToDTO().host_IP)).findFirst().get();
            threadOperations.createThread(theNewProcessThread);
            System.out.println("Process Updated: "+theNewProcessThread.ToDTO().host_name+" | "+theNewProcessThread.ToDTO().host_IP);
        }
    }

    protected void checkIfDeletedOrUpdated(List<SNMPProcess> processList, List<SNMPProcess> lastProcessList) {
        for (SNMPProcess oldProcess : lastProcessList) {
            if (!processList.stream().filter(o -> o.ToDTO().host_IP.equals(oldProcess.ToDTO().host_IP)).findFirst().isPresent()) {
                System.out.println("Process Deleted: "+oldProcess.ToDTO().host_name+" | "+oldProcess.ToDTO().host_IP);
                threadOperations.killThread(oldProcess.ToDTO().host_IP);
            } else {
                checkIfUpdated(processList, oldProcess);
            }
        }
    }

    protected void checkIfAdded(List<SNMPProcess> processList, List<SNMPProcess> lastProcessList) {
        for (SNMPProcess newProcess : processList) {
            if (!lastProcessList.stream().filter(o -> o.ToDTO().host_IP.equals(newProcess.ToDTO().host_IP)).findFirst().isPresent()) {
                System.out.println("New Process Started: "+newProcess.ToDTO().host_name+" | "+newProcess.ToDTO().host_IP);
                threadOperations.createThread(newProcess);
            }
        }
    }
}
